package q

import (
	"fmt"
	"math"
)

// TODO:
// Autoscale of pool size and queue's out size.

// Doesn't work for now

// Scales pool size and out size of queue
type DefaultScaler struct {
	ActorOnTasks int
	MinActors    int
	MaxActors    int
	Pool         *ActorPool
}

func (s *DefaultScaler) Scale(queue *Queue) {
	var queueSize int = len(queue.tasks)
	var poolSize int = s.Pool.GetCount()
	var actorsShouldExist int = int(math.Round(float64(queueSize / s.ActorOnTasks)))
	var difference int = int(math.Abs(float64(poolSize) - float64(actorsShouldExist)))

	if poolSize < actorsShouldExist || poolSize < s.MinActors { // If actors too much
		killCount := poolSize - actorsShouldExist // difference
		s.Pool.Kill(&killCount)
		return
	}

	// If actors not enougth
	var addCount int
	if int(difference)+poolSize > s.MaxActors {
		addCount = s.MaxActors - poolSize
	}
	for i := 0; i < addCount; i++ {
		fmt.Printf("Actor created!\n")
		s.Pool.AddActor()
	}
}
