package q

type Actor struct {
	run chan bool
}

func NewActor() Actor {
	return Actor{
		run: make(chan bool),
	}
}

func (a *Actor) Run(queue *Queue) {
	run := true
	// go func() {
	// 	for run {
	// 		run = <-a.run
	// 	}
	// }()
	for run {
		queue.Next()
		tasks, open := <-queue.Out
		if len(tasks) < 1 {
			continue
		}
		if !open {
			return
		}
		for i := 0; i < len(tasks); i++ {
			(*tasks[i]).Execute()
		}
	}
}

func (a *Actor) Kill() {
	a.run <- false
}
