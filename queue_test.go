package q_test

import (
	"fmt"
	"sync"
	"testing"

	"gitlab.com/4nd3rs0n/q"
)

type Nums struct {
	A int
	B int
}

func TestNewQueue(t *testing.T) {
	nums := []Nums{
		{A: 1, B: 1},
		{A: 2, B: 3},
		{A: 100, B: 5},
		{A: 10, B: -8},
		{A: 4321, B: 1122},
		{A: -1543, B: 3731},
		{A: 1 + 39, B: -109000},
		{A: 1 * 1, B: 1},
	}
	// Init queue and actors
	queue := q.NewQueue()
	pool := q.NewActorPool()
	// Run
	go pool.Run(queue)
	go queue.Run()
	// Kill pool and actors in the end
	defer queue.Kill()
	defer pool.Kill(nil)
	// Add actors to the pool
	for i := 0; i < 3; i++ {
		pool.AddActor()
	}

	// TODO: Autoscale
	// scaler := &q.DefaultScaler{
	// 	ActorOnTasks: 100,
	// 	MinActors:    100,
	// 	MaxActors:    1000,
	// 	Pool:         pool,
	// }
	// queue.Scaler = scaler.Scale

	var wg sync.WaitGroup
	go func() {
		for i := 0; i < len(nums); i++ {
			wg.Add(1)
			getI := i + 1
			testname := fmt.Sprintf("A:%v,B:%v", nums[i].A, nums[i].B)
			excepted := nums[i].A + nums[i].B
			newTask := NewPlusTask(nums[i].A, nums[i].B)
			var task q.Task = newTask

			go t.Run(testname, func(t *testing.T) {
				// Create task
				// Send task intop queue
				queue.In <- &task
				go func() {
					// Wait for result of task
					res := newTask.Result()
					if res != excepted {
						t.Errorf("Excepted: %v, Got: %v", excepted, res)
					}
					fmt.Printf("Ok %v/%v\n", getI, len(nums))
					wg.Done()
				}()
			})
		}
	}()

	wg.Wait()
}

// TODO: Stress test. 500000 random tasks from 3 goroutines.
//
// func randTask() {
// }
//
// func StressQueue(t *testing.B) {
// 	go func() {
// 		for {

// 		}
// 	}()
// 	go func() {
// 		for {

// 		}
// 	}()
// 	go func() {
// 		for {

// 		}
// 	}()
// }
