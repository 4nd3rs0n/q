package q_test

// Simple task that pluses two numbers together
type PlusTask struct {
	a   int
	b   int
	Out chan int
}

// Constructor
//
// Usage:
//
//	plusTask := NewPlusTask(a, b)
//	var task q.Task = plusTask
//	queue.In <- &task
//	fmt.Printf("%d\n" plusTask.Result())
func NewPlusTask(a int, b int) *PlusTask {
	return &PlusTask{
		a:   a,
		b:   b,
		Out: make(chan int),
	}
}

// Returns result of the task execution and block current goroutine while task not executed
func (t PlusTask) Result() int {
	out := <-t.Out
	return out
}

// Will be runned by actor
// Just pluses two numbers that were assigned in constructor
func (t PlusTask) Execute() {
	t.Out <- t.a + t.b
}
