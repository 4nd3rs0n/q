package q

type ActorPool struct {
	actors   []*Actor
	newActor chan *Actor
}

func NewActorPool() *ActorPool {
	newActor := make(chan *Actor)

	pool := &ActorPool{
		actors:   []*Actor{},
		newActor: newActor,
	}
	return pool
}

func (a *ActorPool) Run(queue *Queue) {
	for {
		actor := <-a.newActor
		go (*actor).Run(queue)
	}
}

func (a *ActorPool) AddActor() {
	actor := NewActor()
	a.newActor <- &actor
	a.actors = append(a.actors, &actor)
}

func (a *ActorPool) Kill(count *int) {
	var killCount int
	if count == nil {
		killCount = len(a.actors)
	} else {
		killCount = *count
	}
	for i := 0; i < killCount; i++ {
		a.actors[i].Kill()
	}
}

func (a *ActorPool) GetCount() int {
	return len(a.actors)
}
