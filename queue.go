package q

/*
task => In channel => queue => out channel => anything
*/

// Another one type of queue. It can't be grouped in pools or something liken that.
// It's good solution if you need only one queue
type Queue struct {
	tasks []*Task
	In    chan *Task   // You can add tasks here
	Out   chan []*Task // Tasks that are waiting to be executed will be here
	// TODO: Scaler
	// Scaler  func(*Queue)
	OutSize int // How long
	next    chan bool
}

func NewQueue() *Queue {
	newQueue := Queue{
		In:      make(chan *Task),
		Out:     make(chan []*Task),
		next:    make(chan bool),
		tasks:   []*Task{},
		OutSize: 3,
	}
	return &newQueue
}

func (q *Queue) updateTasks() {
	if len(q.tasks) < 1 {
		for { // Wait for new task
			newTask := <-q.In
			if newTask == nil {
				continue
			}
			// Out this new task
			q.Out <- []*Task{newTask}
			return
		}
	}
	var outSize int = q.OutSize
	if len(q.tasks) < q.OutSize {
		outSize = len(q.tasks)
	}
	q.Out <- q.tasks[:outSize]
	// Remove outed tasks
	q.tasks = q.tasks[outSize:]
}

func (q *Queue) Run() {
	for {
		// TODO: Scaler
		// if q.Scaler != nil {
		// 	q.Scaler(q)
		// }
		select {
		case newTask, open := <-q.In:
			if newTask == nil {
				continue
			}
			if !open {
				return
			}
			q.tasks = append(q.tasks, newTask)
		case next := <-q.next:
			if !next {
				close(q.Out)
				return
			}
			q.updateTasks()
		}
	}
}

func (q *Queue) Next() {
	q.next <- true
}
func (q *Queue) Kill() {
	close(q.In)
	q.next <- false
}
