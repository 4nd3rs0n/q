# Q toolkit
Q is toolkit with some often used in actor architectire components like queues, actors and pool of actors. 

## Task
Task is an interface that has only necessary method -- Execute. Execute can do anything. Execute will be called by actor.
You can see example of task in `task_test.go`.

## Queue
Queue has two channels: in and out. queue.In is a channel which can be used to put pointer to some task. From queue.Out channel we can take tasks. But before take the task we should call queue.Next, it's like we saying to the Queue that it need to prepare new task for us. If there is no tasks in queue -- queue will be waiting for the new task that it can give us.  

```go
package main

import (
  "log"
  "gitlab.com/4nd3rs0n/q"
)

func main() {
  // Create new queue
  queue := q.NewQueue()
  // Listen for a new tasks and outing task
  go queue.Run()
  // Stop running this queue. Also kills all actors
  defer queue.Kill()

  // Adding pointer to the task into queue
  queue.In <- &someTask

  queue.Next() // Ask for a new task in queue.Out
  tasks := <-queue.Out // Get this new task
  if task == nil { // If task is nill -- it can't be executed 
    log.Fatalf("Task is nil and cannot be executed")
  }
  for i := 0; i < len(tasks); i++ {
    (*task).Execute() // Execute this task
  }
}
```

## Actor
Actor can also be named as "Worker" or "Task runner" or anything else. Actor is just component that calls queue.Next(), takes tasks from the queue.Out and than Executes it (and may also run some middleware before or after task executon). In this package there is only simple actor, that don't runs any middleware, but if you want -- you can write your own actor with extended functionality.   

```go
package main

import "gitlab.com/4nd3rs0n/q"

func main() {
  // ... Something ... maybe initializing queue

  // Creates new actor
  actor := q.NewActor()
  // Asks for tasks from queue and executes it
  go actor.Run(*queue)
  defer actor.kill()
}
```

But no one really use just one actor because it's slow.  
It's better to use pool of actors if you what to have good perfomance.

## Pool of actors
Actor pool is like a group of actors that executing tasks from one queue, what makes execution of tasks faster.  

```go
package main

import "gitlab.com/4nd3rs0n/q"

func main() {
  // ... Something ... maybe initializing queue

  // Creates new pool
  pool := q.NewActorPool()
  // Runs all actors in pool and wait for adding new actors
  go pool.Run(*queue)
  
  // Kills all actors in pool
  // You can change nil to the number of actors
  // When queue is killed -- all the actors and actor pools will be killed as well 
  defer actor.Kill(nil)  

  pool.AddActor() // Add default actor to the pool
}
```

## TODO: Autoscale
Autoscale can resize actor pool and out size of queue. It makes out size and bigger and creates new actors when queue getting bigger and make out size smaller and remove unused actors when queue getting smaller.